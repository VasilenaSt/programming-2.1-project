package be.kdg.java2.banksystem.repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JSONCreator {

    public static void saveInJSON(String filename, List data) throws IOException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        FileWriter fileWriter = new FileWriter(filename + ".json");
        fileWriter.write(gson.toJson(data));


    }
}
