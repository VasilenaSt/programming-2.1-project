package be.kdg.java2.banksystemwebapp.repository.jpa;


import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;
/*
@Repository
@Profile("jpa")
public class JPAClientRepository implements EntityRepository<Client> {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    private Logger logger = LoggerFactory.getLogger(JPABankRepository.class);

    public JPAClientRepository() {
    }

    @Override
    public List<Client> read() {
        EntityManager em=entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List clients=em.createQuery("select c from Client c").getResultList();
        em.getTransaction().commit();
        em.close();
        return clients;
    }

    @Override
    public Client create(Client client) {
        EntityManager em=entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(client);
        logger.info("Created client:" + client);
        em.getTransaction().commit();
        em.close();
        return client;
    }

    @Override
    public Client findById(int id) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Client client = em.find(Client.class, id);
        logger.info("Found client:" + client);
        em.getTransaction().commit();
        em.close();
        return client;
    }

    @Override
    public void delete(Client client) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Client clientDB = em.find(Client.class, client.getId());
        em.remove(clientDB);
        logger.info("Deleted client:" + clientDB);
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void update(Client client) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Client clientDB = em.find(Client.class, client.getId());
        clientDB=client;
        logger.info("Updated client:" + client);
        em.getTransaction().commit();
        em.close();
    }
}
*/