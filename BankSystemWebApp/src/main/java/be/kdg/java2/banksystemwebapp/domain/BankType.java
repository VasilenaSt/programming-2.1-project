package be.kdg.java2.banksystemwebapp.domain;

public enum BankType {
    CENTRAL("Central bank"), RETAIL("Retail bank"), COMMERCIAL("Commercial bank"),
    INVESTMENT("Investment bank"), COOPERATIVE("Cooperative bank");

    private String name;

    BankType(String s) {
        this.name = s;
    }

    @Override
    public String toString() {
        return name;
    }
}