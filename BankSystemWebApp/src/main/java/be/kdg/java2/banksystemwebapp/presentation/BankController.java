package be.kdg.java2.banksystemwebapp.presentation;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.BankType;
import be.kdg.java2.banksystemwebapp.domain.Ceo;
import be.kdg.java2.banksystemwebapp.presentation.dto.BankDTO;
import be.kdg.java2.banksystemwebapp.services.BankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/banks")
public class BankController {
    private Logger logger = LoggerFactory.getLogger(BankController.class);
    private BankService bankService;

    public BankController(BankService bankService) {
        logger.debug("creating BankController...");
        this.bankService = bankService;
    }

    @GetMapping
    public String showAllBanks(Model model) {
        logger.debug("showing all banks");
        //model.addAttribute("banks", bankService.getAllBanks());
        model.addAttribute("banks", bankService.findAll());
        return "banks";
    }

    @GetMapping("/addbank")
    public String showAddBank(Model model) {
        logger.debug("showing bank types in addBank form");
        model.addAttribute("bank", new BankDTO());
        model.addAttribute("types", BankType.values());
        return "addbank";
    }

    @PostMapping("/addbank")
    public String handleAddBank(@Valid @ModelAttribute("bank") BankDTO bankDTO, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> logger.error(error.toString()));//show the form again
            model.addAttribute("types", BankType.values());
            return "addbank";
        }
        logger.debug(bankDTO + " is created");
        Bank bank = new Bank(bankDTO.getName(), bankDTO.getType(), bankDTO.getHeadquarters());
        Ceo ceo = new Ceo(bankDTO.getCeoname(), bankDTO.getCeosalary(), bankDTO.getCeosince());
        bank.setCEO(ceo);
        ceo.setManagedBank(bank);
        bankService.addBank(bank);
        return "redirect:/banks";
    }


    @GetMapping("/bankinfo")
    public String showBankInfo(@RequestParam("bankId") Integer bankId, Model model) {
        logger.debug("showing the bank information");
        //Bank bank = bankService.getBankById(bankId);
        Bank bank = bankService.findById(bankId);
        System.out.println(bank.getClients());
        model.addAttribute("bank", bank);
        return "bankinfo";
    }

}
