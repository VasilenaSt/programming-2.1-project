package be.kdg.java2.banksystemspringapp.banksystem.services;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Bank;
import be.kdg.java2.banksystemspringapp.banksystem.domain.BankType;
import be.kdg.java2.banksystemspringapp.banksystem.repository.BankRepository;
import be.kdg.java2.banksystemspringapp.banksystem.repository.JSONSaver;
import be.kdg.java2.banksystemspringapp.banksystem.utils.exceptions.JsonFileBankException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BankServiceImpl implements BankService {
    private BankRepository bankRepository;
    private JSONSaver saver;

    @Autowired
    public BankServiceImpl(BankRepository bankRepository, JSONSaver saver) {
        this.bankRepository = bankRepository;
        this.saver = saver;
    }

    @Override
    public Bank addBank(String name, BankType type, String headquarters) {
        Bank bank = new Bank(name, type, headquarters);
        return bankRepository.createBank(bank);
    }

    @Override
    public List<Bank> getAllBanks() {
        return bankRepository.readBanks();
    }

    @Override
    public List<Bank> getBanksByType(BankType type) {
        return bankRepository.readBanks().stream().filter(bank -> bank.getType() == type).toList();
    }

    @Override
    public void saveBanksInJson(List<Bank> banks) throws JsonFileBankException {
        saver.saveBanks(banks);
    }
}
