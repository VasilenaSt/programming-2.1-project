package be.kdg.java2.banksystem.services;

import be.kdg.java2.banksystem.domain.Client;
import be.kdg.java2.banksystem.repository.BankRepository;

import java.util.List;

public class ClientServiceImpl implements ClientService {

    private BankRepository bankRepository;

    public ClientServiceImpl(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Override
    public Client addClient(String name, int creditScore, String address) {
        Client client = new Client(name, creditScore, address);
        return bankRepository.createClient(client);
    }

    @Override
    public List<Client> getAllClients() {
        return bankRepository.readClients();
    }

    public List<Client> getClientsByNameAndCredit(String name, int creditscore) {
        return bankRepository.readClients().stream()
                .filter(client -> client.getName().contains(name) && client.getCreditScore() > creditscore)
                .toList();
    }
}
