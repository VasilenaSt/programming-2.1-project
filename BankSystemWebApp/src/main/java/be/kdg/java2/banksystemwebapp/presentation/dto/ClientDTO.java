package be.kdg.java2.banksystemwebapp.presentation.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ClientDTO {

    @NotBlank(message = "Name is mandatory")
    @Size(min = 3, max = 100, message = "Name should have length between 2 and 100")
    private String name;

    @NotBlank(message = "Address is mandatory")
    @Size(min = 5, max = 100, message = "Address should have length between 4 and 100")
    private String address;

    @Max(value = 850, message = "You can enter a maximum of 850 credits")
    @Min(value = 300, message = "You can enter a minimum of 300 credits")
    private int creditscore;

    private int bankId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCreditscore() {
        return creditscore;
    }

    public void setCreditscore(int creditscore) {
        this.creditscore = creditscore;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", score=" + creditscore +
                ", bankId=" + bankId +
                '}';
    }
}
