package be.kdg.java2.banksystemwebapp.domain;

import javax.persistence.*;
import java.time.LocalDate;
@javax.persistence.Entity
@Table(name = "ceos")
public class Ceo extends Entity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "salary")
    private double salary;

    @Column(name = "in_office")
    private LocalDate inOffice;

    @OneToOne(mappedBy = "ceo", cascade = CascadeType.ALL)

    private Bank managedBank;

    public Ceo() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ceo(String name, double salary, LocalDate since) {
        this.name = name;
        this.managedBank = null;
        this.salary = salary;
        this.inOffice = since;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public LocalDate getInOffice() {
        return inOffice;
    }

    public void setInOffice(LocalDate ceoSince) {
        this.inOffice = ceoSince;
    }

    public Bank getManagedBank() {
        return managedBank;
    }

    public void setManagedBank(Bank managedBank) {
        this.managedBank = managedBank;
    }

    @Override
    public String toString() {
        return String.format("CEO: %22s  ID: %2d Salary: %f \nIs in office since: %s", name, id, salary, inOffice.toString());
    }
}
