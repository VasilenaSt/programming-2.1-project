package be.kdg.java2.banksystemspringapp.banksystem.services;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Bank;
import be.kdg.java2.banksystemspringapp.banksystem.domain.BankType;
import be.kdg.java2.banksystemspringapp.banksystem.utils.exceptions.JsonFileBankException;

import java.util.List;

public interface BankService {
    Bank addBank(String name, BankType type, String headquarters);

    List<Bank> getAllBanks();

    List<Bank> getBanksByType(BankType type);

    void saveBanksInJson(List<Bank> banks) throws JsonFileBankException;
}
