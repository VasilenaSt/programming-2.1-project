package be.kdg.java2;

import be.kdg.java2.banksystem.presentation.View;
import be.kdg.java2.banksystem.repository.BankRepository;
import be.kdg.java2.banksystem.repository.HardcodedBankRepository;
import be.kdg.java2.banksystem.services.BankService;
import be.kdg.java2.banksystem.services.BankServiceImpl;
import be.kdg.java2.banksystem.services.ClientService;
import be.kdg.java2.banksystem.services.ClientServiceImpl;


public class StartApplication {
    public static void main(String[] args) {

        BankRepository bankRepository = new HardcodedBankRepository();
        BankService bankService = new BankServiceImpl(bankRepository);
        ClientService clientService = new ClientServiceImpl(bankRepository);
        View view = new View(bankService, clientService);
        view.showMenu();
    }
}
