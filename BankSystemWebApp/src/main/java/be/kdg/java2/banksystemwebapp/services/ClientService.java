package be.kdg.java2.banksystemwebapp.services;

import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileClientException;

import java.util.List;

public interface ClientService {
    Client findById(Integer clientId);

    Object findAll();

    void addClient(Client client);

    List<Client> findByCreditScoreGreaterThanEqual(int score);

    List<Client> findByCreditScoreAndName(String name, int score);
    /*
    Client addClient(Client client);

    List<Client> getAllClients();

    List<Client> getClientsByNameAndCredit(String name, int creditscore);

    Client getClientById(int id);

    void saveClientsInJson(List<Client> clients) throws JsonFileClientException;
    */

}
