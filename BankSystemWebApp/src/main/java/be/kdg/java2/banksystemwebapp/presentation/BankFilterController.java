package be.kdg.java2.banksystemwebapp.presentation;

import be.kdg.java2.banksystemwebapp.presentation.dto.FilterBankDTO;
import be.kdg.java2.banksystemwebapp.presentation.dto.FilterClientDTO;
import be.kdg.java2.banksystemwebapp.services.BankService;
import be.kdg.java2.banksystemwebapp.services.ClientService;
import be.kdg.java2.banksystemwebapp.utils.exceptions.FutureDateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.SQLException;

@Controller
@RequestMapping("/filterbanks")
public class BankFilterController {
    private Logger log = LoggerFactory.getLogger(BankFilterController.class);
    private BankService bankService;

    public BankFilterController(BankService bankService) {
        this.bankService = bankService;
    }

    @GetMapping
    public String showFilteredBanks(Model model){
        model.addAttribute("filter", new FilterBankDTO());
        return "filterbanks";
    }

    @PostMapping
    public String handleFilteredBanks(@Valid @ModelAttribute("filter") FilterBankDTO filter, Model model) {
        model.addAttribute("banks", bankService.findByCEOSinceAfter(filter.getInOffice()));
        return "filterbanks";
    }

    @ExceptionHandler(FutureDateException.class)
    public ModelAndView handleError(HttpServletRequest req, FutureDateException exception) {
        log.error(exception.getMessage());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", exception);
        modelAndView.setViewName("futureDateError");
        return modelAndView;
    }
}
