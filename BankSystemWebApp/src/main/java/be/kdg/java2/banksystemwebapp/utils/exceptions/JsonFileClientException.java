package be.kdg.java2.banksystemwebapp.utils.exceptions;

public class JsonFileClientException extends Exception {

    public JsonFileClientException() {
    }

    public JsonFileClientException(String message) {
        super(message);
    }

    public JsonFileClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonFileClientException(Throwable cause) {
        super(cause);
    }
}
