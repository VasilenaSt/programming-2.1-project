package be.kdg.java2.banksystemwebapp.utils.exceptions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

@ControllerAdvice
public class GlobalExceptionHandler {
    private Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    //@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "General exception occured...")
    @ExceptionHandler(value=  Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e){
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it - like the OrderNotFoundException example
        // at the start of this post.
        // AnnotationUtils is a Spring Framework utility class.
        /*
        if (AnnotationUtils.findAnnotation
                (e.getClass(), ResponseStatus.class) != null)
            throw e;
*/
        // Otherwise setup and send the user to a default error-view.
        log.error("Exception: " + e.getMessage());
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        return mav;

    }
    @ExceptionHandler({ SQLException.class, DataAccessException.class })
    public ModelAndView databaseErrorHandler(HttpServletRequest req, Exception e){
        log.error("SQL Exception: " + e.getMessage());
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("databaseError");
        return mav;

    }
}
