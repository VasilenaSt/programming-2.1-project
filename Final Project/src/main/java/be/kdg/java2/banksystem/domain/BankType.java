package be.kdg.java2.banksystem.domain;

public enum BankType {
    CENTRAL("Central bank"), RETAIL("Retail bank"), COMMERCIAL("Commercial bank"),
    INVESTMENT("Investment bank"), COOPERATIVE("Cooperative bank");

    private String name;

    BankType(String s) {
        this.name = s;
    }

    @Override
    public String toString() {
        return name;
    }
}

/**
 * 1. Central Banks:
 * Central banks manage the money supply in a single country or a series of nations.
 * They supervise commercial banks, set interest rates and control the flow of currency.
 * <p>
 * 2. Retail Banks:
 * Retail banks offer members of the public financial products and
 * services such as bank accounts, loans, credit cards and insurance.
 * In some cases, they can set up checking accounts and make loans for
 * small-scale businesses as well.
 * <p>
 * 3. Commercial Banks:
 * Commercial banks tend to concentrate on supporting businesses.
 * Both large corporations and small businesses can turn to commercial
 * banks if they need to open a checking or savings account, borrow money,
 * get access to credit or transfer funds to companies in foreign markets.
 * <p>
 * 4. Investment Banks:
 * Investment Banks manage the trading of stocks, bonds and other securities
 * between companies and investors. On the other hand, they might focus their
 * energy on advising individuals and corporations who need financial guidance,
 * reorganizing companies through mergers and acquisitions, managing investment
 * portfolios or raising money for certain businesses and the federal government.
 * <p>
 * 5. Cooperative Banks:
 * Cooperatives can be either retail banks or commercial banks. They’re typically
 * local or community-based associations whose members help determine how the
 * business is operated. They’re run democratically, and they offer loans and
 * banks accounts, among other things.
 */
/**
 * 2. Retail Banks:
 * Retail banks offer members of the public financial products and
 * services such as bank accounts, loans, credit cards and insurance.
 * In some cases, they can set up checking accounts and make loans for
 * small-scale businesses as well.
 */
/**
 * 3. Commercial Banks:
 * Commercial banks tend to concentrate on supporting businesses.
 * Both large corporations and small businesses can turn to commercial
 * banks if they need to open a checking or savings account, borrow money,
 * get access to credit or transfer funds to companies in foreign markets.
 */
/**
 * 4. Investment Banks:
 * Investment Banks manage the trading of stocks, bonds and other securities
 * between companies and investors. On the other hand, they might focus their
 * energy on advising individuals and corporations who need financial guidance,
 * reorganizing companies through mergers and acquisitions, managing investment
 * portfolios or raising money for certain businesses and the federal government.
 */
/**
 * 5. Cooperative Banks:
 * Cooperatives can be either retail banks or commercial banks. They’re typically
 * local or community-based associations whose members help determine how the
 * business is operated. They’re run democratically, and they offer loans and
 * banks accounts, among other things.
 */