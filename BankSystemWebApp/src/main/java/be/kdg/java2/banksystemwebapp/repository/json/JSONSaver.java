package be.kdg.java2.banksystemwebapp.repository.json;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileBankException;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileClientException;

import java.util.List;

public interface JSONSaver {

    //String getBankFileName();
    //String getClientFileName();
    void saveBanks(List<Bank> banks) throws JsonFileBankException;

    void saveClients(List<Client> clients) throws JsonFileClientException;


}
