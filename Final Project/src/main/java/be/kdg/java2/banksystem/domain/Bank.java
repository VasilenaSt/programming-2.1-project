package be.kdg.java2.banksystem.domain;

import java.util.HashSet;
import java.util.Set;

public class Bank {

    private String name;
    private BankType type;
    private String headquarters;

    private Ceo ceo;
    private transient Set<Client> clients;

    public Bank(String name, BankType type, String headquarters) {
        this.name = name;
        this.type = type;
        this.headquarters = headquarters;
        this.clients = new HashSet<>();
        this.ceo = null;

    }

    public void setCEO(Ceo ceo) {
        this.ceo = ceo;
    }

    public void addClient(Client client) {
        clients.add(client);
    }

    public String getName() {
        return name;
    }

    public BankType getType() {
        return type;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public String getHeadquarters() {
        return headquarters;
    }

    public Ceo getCeo() {
        return ceo;
    }

    @Override
    public String toString() {
        return "Bank's name: " + name + "; " +
                "Type of bank: " + type + "; " +
                "The Headquarters are located in " + headquarters + '.';
    }
}
