package be.kdg.java2.banksystemwebapp.utils;

import be.kdg.java2.banksystemwebapp.domain.BankType;
import org.springframework.core.convert.converter.Converter;

public class StringToBankTypeConverter implements Converter<String, BankType> {
    @Override
    public BankType convert(String source) {
        if (source.toLowerCase().startsWith("cent")) return BankType.CENTRAL;
        if (source.toLowerCase().startsWith("comm")) return BankType.COMMERCIAL;
        if (source.toLowerCase().startsWith("coop")) return BankType.COOPERATIVE;
        if (source.toLowerCase().startsWith("ret")) return BankType.RETAIL;
        if (source.toLowerCase().startsWith("inv")) return BankType.INVESTMENT;
        return BankType.COMMERCIAL;
    }
}
