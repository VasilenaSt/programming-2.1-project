package be.kdg.java2.banksystemwebapp.presentation;

import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.presentation.dto.ClientDTO;
import be.kdg.java2.banksystemwebapp.services.BankService;
import be.kdg.java2.banksystemwebapp.services.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/clients")
public class ClientController {
    private Logger log = LoggerFactory.getLogger(ClientController.class);
    private ClientService clientService;
    private BankService bankService;

    public ClientController(ClientService clientService, BankService bankService) {
        this.bankService = bankService;
        this.clientService = clientService;
    }

    @GetMapping
    public String showAllClients(Model model) {
        log.debug("showing all Clients");
        //model.addAttribute("clients", clientService.getAllClients());
        model.addAttribute("clients", clientService.findAll());

        return "clients";
    }
    /*
    @PostMapping
    public String handleAddClient(@Valid @ModelAttribute("filter") FilterClientDTO filter,Model model) {
        System.out.println(filter.getName());
        clientService.findByNameContaining(filter.getName()).forEach(System.out::println);
        model.addAttribute("clients",clientService.findByNameContaining(filter.getName()));
        return "redirect:/clients";
    }*/

    @GetMapping("/addclient")
    public String showAddClientForm(Model model) {
        log.debug("showing add client form");
        model.addAttribute("bankList", bankService.findAll());
        model.addAttribute("client", new ClientDTO());
        return "addclient";
    }

    @PostMapping("/addclient")
    public String handleAddClient(@Valid @ModelAttribute("client") ClientDTO clientDTO, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> {
                log.error(error.toString());
            });//show the form again
            model.addAttribute("bankList", bankService.findAll());
            return "addclient";
        }

        log.debug("adding a new client " + clientDTO);

        Client client=new Client(clientDTO.getName(),
                clientDTO.getCreditscore(),
                clientDTO.getAddress());
        bankService.addClient(clientDTO.getBankId(),client);
        System.out.println(bankService.findById(clientDTO.getBankId()).getClients());

        /*
        Client client = new Client(clientDTO.getName(), clientDTO.getCreditscore(), clientDTO.getAddress());
        bankService.getAllBanks().get(clientDTO.getBankId() - 1).addClient(client);
        client.addBank(bankService.getAllBanks().get(clientDTO.getBankId() - 1));
        clientService.addClient(client);
        */
        return "redirect:/clients";
    }

    @GetMapping("/clientinfo")
    public String showBankInfo(@RequestParam("clientId") Integer clientId, Model model) {
        log.debug("showing the bank information");
        Client client = clientService.findById(clientId);
        System.out.println(client.getBanks());
        model.addAttribute("client", client);
        return "clientinfo";
    }
}
