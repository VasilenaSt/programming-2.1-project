package be.kdg.java2.banksystem.presentation;

import be.kdg.java2.banksystem.domain.Bank;
import be.kdg.java2.banksystem.domain.BankType;
import be.kdg.java2.banksystem.domain.Client;
import be.kdg.java2.banksystem.services.BankService;
import be.kdg.java2.banksystem.services.ClientService;
import be.kdg.java2.utils.LocalDateSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class View {


    private BankService bankService;
    private ClientService clientService;
    private Scanner sc = new Scanner(System.in);
    private boolean quit = false;
    int answer;
    Gson gson;

    public View(BankService bankService, ClientService clientService) {
        this.bankService = bankService;
        this.clientService = clientService;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
        gsonBuilder.setPrettyPrinting();
        this.gson = gsonBuilder.create();
        answer = -1;
    }

    public void showMenu() {
        while (!quit) {
            int answer = -1;
            System.out.println();
            System.out.println("What would you like to do?");
            System.out.println("==========================");
            System.out.println("0) Quit");
            System.out.println("1) Show all banks");
            System.out.println("2) Show banks of a specific type");
            System.out.println("3) Show all clients");
            System.out.println("4) Show clients with name and credit score above the given ");
            System.out.println("Choice (0-4):");
            if (sc.hasNext()) {
                answer = sc.nextInt();
            }
            handleChoice(answer);
        }
    }

    private void handleChoice(int choice) {
        switch (choice) {
            case 0 -> quit = true;
            case 1 -> showAllBanks();
            case 2 -> showTypeBanks();
            case 3 -> showAllClients();
            case 4 -> showFilteredClients();
            default -> System.out.println("Invalid output!\n");
        }
    }

    private void showFilteredClients() {

        System.out.println("Enter (part of) a name or leave blank:");
        sc.nextLine();
        String name = sc.nextLine();

        System.out.println("Enter credit score amount or leave blank:");
        int score = 0;
        String scoreStr = sc.nextLine();
        try {
            score = Integer.parseInt(scoreStr);
        } catch (NumberFormatException e) {
            System.out.println("Invalid number format! Score is 0 by default.");
        }
        List<Client> filteredClients = clientService.getClientsByNameAndCredit(name, score);
        filteredClients.forEach(System.out::println);
        try (
                FileWriter fileWriter = new FileWriter("clients.json")) {
            fileWriter.write(gson.toJson(filteredClients));
            System.out.println("Data is saved to clients.json...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showAllClients() {
        clientService.getAllClients().stream().forEach(System.out::println);
        try (
                FileWriter fileWriter = new FileWriter("clients.json")) {
            fileWriter.write(gson.toJson(clientService.getAllClients()));
            System.out.println("Data is saved to clients.json...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showTypeBanks() {
        System.out.println("Type (1=Central Banks, 2=Retail Banks, 3=Commercial Banks," +
                " 4=Investment Banks, 5=Cooperative Banks):");
        if (sc.hasNext()) {
            answer = sc.nextInt();
        }
        while (answer < 1 || answer > 5) {
            System.out.println("Invalid output! Please, enter your answer again.");
            if (sc.hasNext()) {
                answer = sc.nextInt();
            }
        }
        List<Bank> filteredBanks = new ArrayList<>();

        switch (answer) {
            case 1:
                filteredBanks = bankService.getBanksByType(BankType.CENTRAL);
                break;
            case 2:
                filteredBanks = bankService.getBanksByType(BankType.RETAIL);
                break;
            case 3:
                filteredBanks = bankService.getBanksByType(BankType.COMMERCIAL);
                break;
            case 4:
                filteredBanks = bankService.getBanksByType(BankType.INVESTMENT);
                break;
            case 5:
                filteredBanks = bankService.getBanksByType(BankType.COOPERATIVE);
                break;
        }
        filteredBanks.forEach(System.out::println);
        //
        try (FileWriter fileWriter = new FileWriter("banks.json")) {
            fileWriter.write(gson.toJson(filteredBanks));
            System.out.println("Data is saved to banks.json...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showAllBanks() {
        bankService.getAllBanks().stream().forEach(System.out::println);
        try (
                FileWriter fileWriter = new FileWriter("banks.json")) {
            fileWriter.write(gson.toJson(bankService.getAllBanks()));
            System.out.println("Data is saved to banks.json...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
