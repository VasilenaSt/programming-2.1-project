package be.kdg.java2.banksystemspringapp.banksystem.bootstrap;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Bank;
import be.kdg.java2.banksystemspringapp.banksystem.domain.BankType;
import be.kdg.java2.banksystemspringapp.banksystem.domain.Ceo;
import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;
import be.kdg.java2.banksystemspringapp.banksystem.repository.BankRepository;
import be.kdg.java2.banksystemspringapp.banksystem.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataSeeder implements CommandLineRunner {
    private BankRepository bankRepository;
    private ClientRepository clientRepository;
    private Logger logger = LoggerFactory.getLogger(DataSeeder.class);

    public DataSeeder(BankRepository bankRepository, ClientRepository clientRepository) {
        this.bankRepository = bankRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.debug("Seeding the data");
        List<Bank> banks = new ArrayList<>();
        List<Client> clients = new ArrayList<>();

        Bank bank1 = new Bank("BNP Paribas Fortis", BankType.COMMERCIAL, "Brussels");
        Ceo ceo1 = new Ceo("Maxime Jadot", 70000.00, LocalDate.of(2010, Month.JANUARY, 1));
        bank1.setCEO(ceo1);
        ceo1.setManagedBank(bank1);
        banks.add(bank1);

        Bank bank2 = new Bank("Central bank of Luxembourg", BankType.CENTRAL, "Luxembourg");
        Ceo ceo2 = new Ceo("Pierre Ahlborn", 80000.00, LocalDate.of(1999, Month.DECEMBER, 1));
        bank2.setCEO(ceo2);
        ceo2.setManagedBank(bank2);
        banks.add(bank2);

        Bank bank3 = new Bank("Goldman Sachs", BankType.INVESTMENT, "New York");
        Ceo ceo3 = new Ceo("David M. Solomon", 170000.00, LocalDate.of(2018, Month.OCTOBER, 1));
        bank3.setCEO(ceo3);
        ceo3.setManagedBank(bank3);
        banks.add(bank3);

        Bank bank4 = new Bank("JPMorgan Chase", BankType.INVESTMENT, "New York");
        Ceo ceo4 = new Ceo("Jamie Dimon", 130000.00, LocalDate.of(2005, Month.DECEMBER, 31));
        bank4.setCEO(ceo4);
        ceo4.setManagedBank(bank4);
        banks.add(bank4);

        Bank bank5 = new Bank("ING Group", BankType.COMMERCIAL, "Amsterdam");
        Ceo ceo5 = new Ceo("Erik Van Den Eynden", 80000.00, LocalDate.of(2017, Month.MARCH, 1));
        bank5.setCEO(ceo5);
        ceo5.setManagedBank(bank5);
        banks.add(bank5);

        Client client1 = new Client("Lauren Bennett", 350, "2786 Werninger Street, Houston,Texas, USA");
        client1.addBank(bank1);
        bank1.addClient(client1);
        clients.add(client1);

        Client client2 = new Client("George Cooper", 653, "2353 Quarry Drive, Montgomery,Alabama, USA");
        client2.addBank(bank1);
        bank1.addClient(client2);
        clients.add(client2);

        Client client3 = new Client("Liza King", 539, "4589 Fleming Street, Mongomary, Alabama, USA");
        client3.addBank(bank1);
        bank1.addClient(client3);
        client3.addBank(bank5);
        bank5.addClient(client3);
        clients.add(client3);

        Client client4 = new Client("Frazer Harrison", 706, "Noordstraat 171, Bomal-sur-Ourthe, Belgium");
        client4.addBank(bank1);
        bank1.addClient(client4);
        client4.addBank(bank5);
        bank5.addClient(client4);
        client4.addBank(bank2);
        bank2.addClient(client4);
        clients.add(client4);

        Client client5 = new Client("Larry Graham", 499, "Lambrechtstraat 297, Westmalle, Belgium");
        client5.addBank(bank5);
        bank5.addClient(client5);
        clients.add(client5);

        Client client6 = new Client("Cloe Weaver", 795, "Rue des Honnelles 124, Zandvoorde, Belgium");
        client6.addBank(bank5);
        bank5.addClient(client6);
        clients.add(client6);

        Client client7 = new Client("Brenda Williamson", 734, "Netelaan 1135, Kolmont, Belgium");
        client7.addBank(bank5);
        bank5.addClient(client7);
        client7.addBank(bank2);
        bank2.addClient(client7);
        clients.add(client7);


        Client client8 = new Client("Jerry Drew Associates", 832, "Orchideeenlaan 129, Merchtem, Belgium");
        client8.addBank(bank3);
        bank3.addClient(client8);
        clients.add(client8);

        Client client9 = new Client("Baker Corp", 849, "82 Manor Close, Dilton, UK");
        client9.addBank(bank3);
        bank3.addClient(client9);
        clients.add(client9);

        Client client10 = new Client("Carter Industries", 799, "58 Bridge Street, Gorllwyn, UK");
        client10.addBank(bank3);
        bank3.addClient(client10);
        client10.addBank(bank4);
        bank4.addClient(client10);
        clients.add(client10);

        Client client11 = new Client("Raven and Co", 802, "119 New Dover Rd, Walditch, UK");
        client11.addBank(bank4);
        bank4.addClient(client11);
        clients.add(client11);

        Client client12 = new Client("Tesla, Inc.", 850, "3500 DEER CREEK RD PALO, California, USA");
        client12.addBank(bank4);
        bank4.addClient(client12);
        clients.add(client12);

        banks.forEach(bankRepository::createBank);
        clients.forEach(clientRepository::createClient);
    }
}
