package be.kdg.java2.banksystem.repository;

import be.kdg.java2.banksystem.domain.Bank;
import be.kdg.java2.banksystem.domain.Ceo;
import be.kdg.java2.banksystem.domain.Client;

import java.util.List;

public interface BankRepository {
    List<Bank> readBanks();

    List<Client> readClients();

    List<Ceo> readCEOs();

    Bank createBank(Bank bank);

    Client createClient(Client client);

    Ceo createCEO(Ceo ceo);
}
