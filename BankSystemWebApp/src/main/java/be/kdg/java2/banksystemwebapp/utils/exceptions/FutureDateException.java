package be.kdg.java2.banksystemwebapp.utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST,reason="Invalid date")
public class FutureDateException extends RuntimeException{
    public FutureDateException(String message) {
        super(message);
    }
}
