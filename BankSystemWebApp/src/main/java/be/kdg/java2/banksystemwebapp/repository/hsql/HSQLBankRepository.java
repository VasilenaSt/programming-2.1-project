package be.kdg.java2.banksystemwebapp.repository.hsql;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.BankType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
@Repository
@Profile("hsql")
public class HSQLBankRepository implements EntityRepository<Bank> {
    private Logger logger = LoggerFactory.getLogger(HSQLBankRepository.class);

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert bankInserter;


    public HSQLBankRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.bankInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("BANKS").usingGeneratedKeyColumns("id");
        logger.debug("Creating Bank repository with HSQL...");
    }

    public Bank findById(int id) {
        logger.debug("find bank by id with HSQL...");
        return jdbcTemplate.queryForObject("SELECT * FROM PERSONS WHERE id = ?",
                this::mapRow, id);
    }

    @Override
    public void delete(Bank bank) {

        String name = bank.getName();
        logger.debug("Banks with name" + name + " was deleted successfully!");
    }

    @Override
    public void update(Bank bank) {

    }


    private Bank mapRow(ResultSet rs, int i) throws SQLException {
        Bank bank = new Bank(rs.getString("name")
                , BankType.valueOf(rs.getString("type").toUpperCase())
                , rs.getString("headquarters"));
        return bank;
    }

    @Override
    public List<Bank> read() {
        logger.debug("read banks with HSQL...");
        return jdbcTemplate.query("SELECT * FROM BANKS", this::mapRow);
    }

    @Override
    public Bank create(Bank bank) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", bank.getName());
        parameters.put("TYPE", bank.getType().toString());
        parameters.put("HEADQUARTERS", bank.getHeadquarters());
        bank.setId(bankInserter.executeAndReturnKey(parameters).intValue());
        logger.info("Bank with name: " + bank.getName() +
                " is saved with id: " + bank.getId());
        return bank;
    }


}*/
