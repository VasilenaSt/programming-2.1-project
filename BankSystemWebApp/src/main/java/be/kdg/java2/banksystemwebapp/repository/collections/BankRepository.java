package be.kdg.java2.banksystemwebapp.repository.collections;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.BankType;
import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.domain.Entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/*
@Repository
@Profile("collections")
public class BankRepository extends ListRepository<Bank> {
    private Logger logger = LoggerFactory.getLogger(BankRepository.class);

    public BankRepository() {
        logger.debug("Creating Bank repository...");
    }


}*/
