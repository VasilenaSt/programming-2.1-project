package be.kdg.java2.banksystemspringapp.banksystem.repository;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Bank;
import be.kdg.java2.banksystemspringapp.banksystem.domain.Ceo;
import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;

import java.util.List;

public interface BankRepository {
    List<Bank> readBanks();

    Bank createBank(Bank bank);

}
