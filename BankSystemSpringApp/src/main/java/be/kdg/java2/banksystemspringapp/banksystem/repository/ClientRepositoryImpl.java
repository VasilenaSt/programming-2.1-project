package be.kdg.java2.banksystemspringapp.banksystem.repository;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ClientRepositoryImpl implements ClientRepository {
    private List<Client> clients = new ArrayList<>();

    @Override
    public List<Client> readClients() {
        return clients;
    }

    @Override
    public Client createClient(Client client) {
        clients.add(client);
        return client;
    }
}
