package be.kdg.java2.banksystemspringapp.banksystem.utils.exceptions;

public class JsonFileBankException extends Exception {
    public JsonFileBankException() {
    }

    public JsonFileBankException(String message) {
        super(message);
    }

    public JsonFileBankException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonFileBankException(Throwable cause) {
        super(cause);
    }

}
