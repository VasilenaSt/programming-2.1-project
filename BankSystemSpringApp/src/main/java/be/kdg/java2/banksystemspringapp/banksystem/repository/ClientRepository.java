package be.kdg.java2.banksystemspringapp.banksystem.repository;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;

import java.util.List;

public interface ClientRepository {

    List<Client> readClients();

    Client createClient(Client client);
}
