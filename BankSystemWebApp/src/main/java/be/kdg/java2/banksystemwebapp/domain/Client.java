package be.kdg.java2.banksystemwebapp.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@javax.persistence.Entity
@Table(name="clients")
public class Client extends Entity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "credit_score")
    private int creditScore;

    @Column(name = "address")
    private String address;


    @ManyToMany(mappedBy = "clients", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},fetch = FetchType.EAGER)
    private  List<Bank> banks;

    public Client() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Client(String name, int creditScore, String address) {
        this.name = name;
        this.creditScore = creditScore;
        this.banks = new ArrayList<>();
        this.address = address;
    }

    public void addBank(Bank bank) {
        banks.add(bank);
    }

    public int getCreditScore() {
        return creditScore;
    }

    public List<Bank> getBanks() {
        return banks;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreditScore(int creditScore) {
        this.creditScore = creditScore;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }


    @Override
    public String toString() {
        return String.format("Client: %22s  ID: %2d Credit Score: %d Address: %s", name, id, creditScore, address);


    }
}
