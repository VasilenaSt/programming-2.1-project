package be.kdg.java2.banksystem.repository;

import be.kdg.java2.banksystem.domain.Bank;
import be.kdg.java2.banksystem.domain.Ceo;
import be.kdg.java2.banksystem.domain.Client;

import java.util.ArrayList;
import java.util.List;

public class HardcodedBankRepository implements BankRepository {
    private static List<Bank> banks = new ArrayList<>();
    private static List<Client> clients = new ArrayList<>();
    private static List<Ceo> ceos = new ArrayList<>();

    static {
        DataFactory.seed();
        banks = DataFactory.banks;
        clients = DataFactory.clients;
    }


    @Override
    public List<Bank> readBanks() {
        return banks;
    }

    @Override
    public List<Client> readClients() {
        return clients;
    }

    @Override
    public List<Ceo> readCEOs() {
        return ceos;
    }

    @Override
    public Bank createBank(Bank bank) {
        banks.add(bank);
        return bank;
    }

    @Override
    public Client createClient(Client client) {
        clients.add(client);
        return client;
    }

    @Override
    public Ceo createCEO(Ceo ceo) {
        ceos.add(ceo);
        return ceo;
    }
}
