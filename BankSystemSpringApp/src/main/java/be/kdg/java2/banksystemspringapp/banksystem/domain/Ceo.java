package be.kdg.java2.banksystemspringapp.banksystem.domain;

import java.time.LocalDate;

public class Ceo {
    private String name;
    private double salary;
    private LocalDate ceoSince;

    private transient Bank managedBank;

    public Ceo(String name, double salary, LocalDate since) {
        this.name = name;
        this.managedBank = null;
        this.salary = salary;
        this.ceoSince = since;
    }

    public void setManagedBank(Bank managedBank) {
        this.managedBank = managedBank;
    }

    public String getName() {
        return name;
    }

    public Bank getManagedBank() {
        return managedBank;
    }

    public double getSalary() {
        return salary;
    }

    public LocalDate getCeoSince() {
        return ceoSince;
    }

    @Override
    public String toString() {
        return "Ceo{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", ceoSince=" + ceoSince +
                ", managedBank=" + managedBank +
                '}';
    }
}
