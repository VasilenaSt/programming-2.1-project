package be.kdg.java2.banksystemwebapp.repository.springdatajpa;

import be.kdg.java2.banksystemwebapp.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client,Integer> {


    List<Client> findByCreditScoreGreaterThanEqual(int score);

    @Query("SELECT c FROM Client c WHERE c.name LIKE %?1% AND c.creditScore>=?2")
    List<Client> findByCreditScoreAndName(String name,int creditScore);
}
