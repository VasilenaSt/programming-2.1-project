package be.kdg.java2.banksystemwebapp.presentation.dto;

import be.kdg.java2.banksystemwebapp.domain.BankType;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class BankDTO {
    @NotBlank(message = "Bank's name is mandatory")
    @Size(min = 3, max = 100, message = "Name should have length between 2 and 100")
    private String name;

    private BankType type;

    @NotBlank(message = "Headquarter is mandatory")
    @Size(min = 3, max = 100, message = "Headquarter should have length between 2 and 100")
    private String headquarters;

    @NotBlank(message = "Chief executive's name is mandatory")
    @Size(min = 3, max = 100, message = "Name should have length between 2 and 100")
    private String ceoname;

    @Min(value = 30000, message = "You can enter a minimum of 30 000 $ annual salary")
    private double ceosalary;

    @NotNull(message = "CEO's start date is mandatory")
    @Past(message = "The start date should be in the past")
    private LocalDate ceosince;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BankType getType() {
        return type;
    }

    public void setType(BankType type) {
        this.type = type;
    }

    public String getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(String headquarters) {
        this.headquarters = headquarters;
    }

    public String getCeoname() {
        return ceoname;
    }

    public void setCeoname(String ceoname) {
        this.ceoname = ceoname;
    }

    public double getCeosalary() {
        return ceosalary;
    }

    public void setCeosalary(double ceosalary) {
        this.ceosalary = ceosalary;
    }

    public LocalDate getCeosince() {
        return ceosince;
    }

    public void setCeosince(LocalDate ceosince) {
        this.ceosince = ceosince;
    }

    @Override
    public String toString() {
        return "BankDTO{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", headquarters='" + headquarters + '\'' +
                ", ceoname='" + ceoname + '\'' +
                ", ceosalary='" + ceosalary + '\'' +
                ", ceosince=" + ceosince +
                '}';
    }
}
