package be.kdg.java2.banksystemspringapp.banksystem.services;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;
import be.kdg.java2.banksystemspringapp.banksystem.repository.BankRepository;
import be.kdg.java2.banksystemspringapp.banksystem.repository.ClientRepository;
import be.kdg.java2.banksystemspringapp.banksystem.repository.JSONSaver;
import be.kdg.java2.banksystemspringapp.banksystem.utils.exceptions.JsonFileClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;
    private JSONSaver saver;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository, JSONSaver saver) {
        this.clientRepository = clientRepository;
        this.saver = saver;
    }

    @Override
    public Client addClient(String name, int creditScore, String address) {
        Client client = new Client(name, creditScore, address);
        return clientRepository.createClient(client);
    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.readClients();
    }

    public List<Client> getClientsByNameAndCredit(String name, int creditscore) {
        return clientRepository.readClients().stream()
                .filter(client -> client.getName().contains(name) && client.getCreditScore() > creditscore)
                .toList();
    }

    @Override
    public void saveClientsInJson(List<Client> clients) throws JsonFileClientException {
        saver.saveClients(clients);
    }
}
