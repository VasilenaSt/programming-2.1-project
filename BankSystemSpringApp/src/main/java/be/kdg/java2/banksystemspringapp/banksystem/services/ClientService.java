package be.kdg.java2.banksystemspringapp.banksystem.services;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;
import be.kdg.java2.banksystemspringapp.banksystem.utils.exceptions.JsonFileClientException;

import java.util.List;

public interface ClientService {
    Client addClient(String name, int creditScore, String address);

    List<Client> getAllClients();

    List<Client> getClientsByNameAndCredit(String name, int creditscore);

    void saveClientsInJson(List<Client> clients) throws JsonFileClientException;

}
