package be.kdg.java2.banksystemspringapp;

import be.kdg.java2.banksystemspringapp.banksystem.presentation.View;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BankSystemSpringAppApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =
                SpringApplication.run(BankSystemSpringAppApplication.class, args);
        View view = context.getBean(View.class);
        view.showMenu();
        context.close();

    }

}
