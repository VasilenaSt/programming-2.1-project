package be.kdg.java2.banksystemwebapp.repository.springdatajpa;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface BankRepository extends JpaRepository<Bank,Integer> {
    List<Bank> findByCeoInOfficeAfter(LocalDate inOffice);
}
