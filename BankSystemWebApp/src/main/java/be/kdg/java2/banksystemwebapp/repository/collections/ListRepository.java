package be.kdg.java2.banksystemwebapp.repository.collections;

import be.kdg.java2.banksystemwebapp.domain.Entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
/*
public abstract class ListRepository<T extends Entity> implements EntityRepository<T> {
    private static final Logger log = LoggerFactory.getLogger(ListRepository.class);
    protected List<T> entities = new ArrayList<>();

    public ListRepository() {
        log.debug("Creating repository...");
    }

    @Override
    public List<T> read() {
        log.debug("Reading entities...");
        return entities;
    }

    @Override
    public T findById(int id) {
        return entities.stream().filter(e -> e.getId() == id).findFirst().get();
    }

    @Override
    public T create(T entity) {
        int maxId = entities.stream().mapToInt(Entity::getId).max().orElse(-1);
        entity.setId(maxId + 1);
        log.debug("Creating enity:" + entity + " in " + this);
        entities.add(entity);
        return entity;
    }

    @Override
    public void delete(T entity) {
        log.debug("Deleting enity:" + entity);
        entities.remove(entity);
    }

    @Override
    public void update(T entity) {
        log.debug("Updating enity:" + entity);
        int index = entities.indexOf(entity);
        entities.set(index, entity);
    }
}
*/