package be.kdg.java2.banksystem.services;

import be.kdg.java2.banksystem.domain.Bank;
import be.kdg.java2.banksystem.domain.BankType;
import be.kdg.java2.banksystem.repository.BankRepository;

import java.util.List;

public class BankServiceImpl implements BankService {
    private BankRepository bankRepository;

    public BankServiceImpl(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Override
    public Bank addBank(String name, BankType type, String headquarters) {
        Bank bank = new Bank(name, type, headquarters);
        return bankRepository.createBank(bank);
    }

    @Override
    public List<Bank> getAllBanks() {
        return bankRepository.readBanks();
    }

    @Override
    public List<Bank> getBanksByType(BankType type) {
        return bankRepository.readBanks().stream().filter(bank -> bank.getType() == type).toList();
    }
}
