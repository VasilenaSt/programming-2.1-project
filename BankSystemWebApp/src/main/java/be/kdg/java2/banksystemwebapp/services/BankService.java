package be.kdg.java2.banksystemwebapp.services;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.BankType;
import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileBankException;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public interface BankService {

    Object findAll();

    void addBank(Bank bank);

    Bank findById(Integer bankId);

    Client addClient(int bankId, Client client);

    List<Bank> findByCEOSinceAfter(LocalDate inOffice);
}
/*
public interface BankService {
    Bank addBank(Bank bank);

    List<Bank> getAllBanks();

    List<Bank> getBanksByType(BankType type);

    Bank getBankById(int id);

}
*/
