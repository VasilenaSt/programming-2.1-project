package be.kdg.java2.banksystemwebapp.repository.json;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.utils.LocalDateSerializer;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileBankException;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileClientException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Component
public class JSONSaverImpl implements JSONSaver {
    private Gson gson;
    private Logger logger = LoggerFactory.getLogger(JSONSaverImpl.class);

    public JSONSaverImpl() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
        gsonBuilder.setPrettyPrinting();
        this.gson = gsonBuilder.create();
    }


    @Override
    public void saveBanks(List<Bank> banks) throws JsonFileBankException {
        /*
        String json = gson.toJson(banks);
        try (
                FileWriter fileWriter = new FileWriter("banks.json")) {
            fileWriter.write(json);
            logger.debug("Data is saved to banks.json...");
            //System.out.println("Data is saved to banks.json...");
        } catch (IOException e) {
            throw new JsonFileBankException("Unable to save banks to JSON", e);
        }

         */
    }


    @Override
    public void saveClients(List<Client> clients) throws JsonFileClientException {
        /*
        String json = gson.toJson(clients);
        try (
                FileWriter fileWriter = new FileWriter("clients.json")) {
            fileWriter.write(json);
            logger.debug("Data is saved to clients.json...");
            //System.out.println("Data is saved to clients.json...");
        } catch (IOException e) {
            throw new JsonFileClientException("Unable to save clients to JSON", e);
        }*/
    }
}
