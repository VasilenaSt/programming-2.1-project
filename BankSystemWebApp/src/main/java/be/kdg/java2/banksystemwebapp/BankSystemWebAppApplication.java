package be.kdg.java2.banksystemwebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankSystemWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankSystemWebAppApplication.class, args);
    }

}
