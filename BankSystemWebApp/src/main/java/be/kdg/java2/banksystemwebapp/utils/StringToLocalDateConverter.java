package be.kdg.java2.banksystemwebapp.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class StringToLocalDateConverter implements Converter<String, LocalDate> {
    private Logger logger = LoggerFactory.getLogger(StringToLocalDateConverter.class);

    @Override
    public LocalDate convert(String value) {
        logger.debug("My converter is used; convert to " + value);

        try {
            return LocalDate.parse(value);
        } catch (DateTimeParseException e) {
            return null;
        }
    }


}
