package be.kdg.java2.banksystemwebapp.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@javax.persistence.Entity
@Table(name = "banks")
public class Bank extends Entity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name ="name")
    private String name;

    @Enumerated(EnumType.STRING)
    private BankType type;

    @Column(name = "headquarters")
    private String headquarters;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="ceo_id")
    private Ceo ceo;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},fetch = FetchType.EAGER)
    @JoinTable(name="bank_client",
            joinColumns = @JoinColumn(name = "bank_id"),
            inverseJoinColumns = @JoinColumn(name="client_id"))
    private  List<Client> clients;

    public Bank() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bank(String name, BankType type, String headquarters) {
        this.name = name;
        this.type = type;
        this.headquarters = headquarters;
        this.clients = new ArrayList<>();
        this.ceo = null;

    }

    public void setCEO(Ceo ceo) {
        this.ceo = ceo;
    }

    public void addClient(Client client) {
        clients.add(client);
    }

    public BankType getType() {
        return type;
    }

    public List<Client> getClients() {
        return clients;
    }

    public String getHeadquarters() {
        return headquarters;
    }

    public String getName() {
        return name;
    }

    public Ceo getCeo() {
        return ceo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(BankType type) {
        this.type = type;
    }

    public void setHeadquarters(String headquarters) {
        this.headquarters = headquarters;
    }

    public void setCeo(Ceo ceo) {
        this.ceo = ceo;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    @Override
    public String toString() {
        return String.format("Bank: %26s  ID: %2d Type: %11s Headquarters: %s", name, id, type, headquarters);

    }
}
