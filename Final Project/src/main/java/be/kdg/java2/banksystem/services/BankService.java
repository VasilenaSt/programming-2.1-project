package be.kdg.java2.banksystem.services;

import be.kdg.java2.banksystem.domain.Bank;
import be.kdg.java2.banksystem.domain.BankType;

import java.util.List;

public interface BankService {
    Bank addBank(String name, BankType type, String headquarters);

    List<Bank> getAllBanks();

    List<Bank> getBanksByType(BankType type);
}
