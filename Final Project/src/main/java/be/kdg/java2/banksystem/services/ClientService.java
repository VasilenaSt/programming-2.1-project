package be.kdg.java2.banksystem.services;

import be.kdg.java2.banksystem.domain.Client;

import java.util.List;

public interface ClientService {
    Client addClient(String name, int creditScore, String address);

    List<Client> getAllClients();

    List<Client> getClientsByNameAndCredit(String name, int creditscore);
}
