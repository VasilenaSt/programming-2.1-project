package be.kdg.java2.banksystemspringapp.banksystem.repository;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Bank;
import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;
import be.kdg.java2.banksystemspringapp.banksystem.utils.exceptions.JsonFileBankException;
import be.kdg.java2.banksystemspringapp.banksystem.utils.exceptions.JsonFileClientException;

import java.util.List;

public interface JSONSaver {

    //String getBankFileName();
    //String getClientFileName();
    void saveBanks(List<Bank> banks) throws JsonFileBankException;

    void saveClients(List<Client> clients) throws JsonFileClientException;


}
