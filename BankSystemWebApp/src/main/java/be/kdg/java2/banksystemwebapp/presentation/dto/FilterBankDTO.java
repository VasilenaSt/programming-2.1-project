package be.kdg.java2.banksystemwebapp.presentation.dto;

import java.time.LocalDate;

public class FilterBankDTO {
    private LocalDate inOffice;

    public LocalDate getInOffice() {
        return inOffice;
    }

    public void setInOffice(LocalDate inOffice) {
        this.inOffice = inOffice;
    }
}
