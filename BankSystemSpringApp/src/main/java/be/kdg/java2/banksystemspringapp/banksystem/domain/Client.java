package be.kdg.java2.banksystemspringapp.banksystem.domain;

import java.util.HashSet;
import java.util.Set;

public class Client {
    private String name;
    private int creditScore;
    private String address;

    private transient Set<Bank> banks;


    public Client(String name, int creditScore, String address) {
        this.name = name;
        this.creditScore = creditScore;
        this.banks = new HashSet<>();
        this.address = address;
    }

    public void addBank(Bank bank) {
        banks.add(bank);
    }

    public String getName() {
        return name;
    }

    public int getCreditScore() {
        return creditScore;
    }

    public Set<Bank> getBanks() {
        return banks;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Client " + name +
                " with credit score of " + creditScore +
                " and address '" + address + '\'';
        //", banks=" + banks +

    }
}
