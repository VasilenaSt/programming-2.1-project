package be.kdg.java2.banksystemspringapp.banksystem.repository;

import be.kdg.java2.banksystemspringapp.banksystem.domain.Bank;
import be.kdg.java2.banksystemspringapp.banksystem.domain.Ceo;
import be.kdg.java2.banksystemspringapp.banksystem.domain.Client;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BankRepositoryImpl implements BankRepository {
    private static List<Bank> banks = new ArrayList<>();

    @Override
    public List<Bank> readBanks() {
        return banks;
    }


    @Override
    public Bank createBank(Bank bank) {
        banks.add(bank);
        return bank;
    }


}
