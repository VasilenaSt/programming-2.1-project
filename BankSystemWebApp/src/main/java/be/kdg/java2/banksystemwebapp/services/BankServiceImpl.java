package be.kdg.java2.banksystemwebapp.services;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.BankType;
import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.repository.json.JSONSaver;
import be.kdg.java2.banksystemwebapp.repository.springdatajpa.BankRepository;
import be.kdg.java2.banksystemwebapp.utils.exceptions.FutureDateException;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileBankException;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Service
public class BankServiceImpl implements BankService {
    private BankRepository bankRepository;

    public BankServiceImpl(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Override
    public Object findAll() {
        return bankRepository.findAll();
    }

    @Override
    public void addBank(Bank bank) {
        bankRepository.save(bank);
    }

    @Override
    public Bank findById(Integer bankId) {
        return bankRepository.findById(bankId).orElseThrow();
    }

    @Override
    public Client addClient(int bankId, Client client) {
        Bank bank = findById(bankId);
        bank.addClient(client);
        client.addBank(bank);
        bankRepository.save(bank);
        return client;
    }

    @Override
    @Transactional
    public List<Bank> findByCEOSinceAfter(LocalDate inOffice){
        if(inOffice.isAfter(LocalDate.now())){
            //throw new SQLException("test exception");
            throw new FutureDateException("Date is in the future");
        }
        return bankRepository.findByCeoInOfficeAfter(inOffice);
    }

    /*
    private EntityRepository<Bank> bankRepository;
    private JSONSaver saver;
    private Logger logger = LoggerFactory.getLogger(BankServiceImpl.class);

    @Autowired
    public BankServiceImpl(EntityRepository<Bank> bankRepository, JSONSaver saver) {
        this.bankRepository = bankRepository;
        this.saver = saver;
    }

    @Override
    public Bank addBank(Bank bank) {
        logger.debug("Bank is added");
        //Bank bank = new Bank(name, type,headquarters);
        return bankRepository.create(bank);
    }

    public Bank getBankById(int id) {
        return bankRepository.findById(id);
    }

    @Override
    public List<Bank> getAllBanks() {
        logger.debug("All banks are returned");
        try {
            saver.saveBanks(bankRepository.read());
        } catch (JsonFileBankException e) {
            e.printStackTrace();
        }

        return bankRepository.read();
    }

    @Override
    public List<Bank> getBanksByType(BankType type) {
        logger.debug("Banks are filtered by type");
        List<Bank> filtered = bankRepository.read().stream().filter(bank -> bank.getType() == type).toList();
        try {
            saver.saveBanks(filtered);
        } catch (JsonFileBankException e) {
            e.printStackTrace();
        }
        return filtered;
    }
    */
}
