package be.kdg.java2.banksystemwebapp.repository.jpa;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;
/*
@Repository
@Profile("jpa")
public class JPABankRepository implements EntityRepository<Bank> {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    private Logger logger = LoggerFactory.getLogger(JPABankRepository.class);

    public JPABankRepository() {
    }

    @Override
    public List<Bank> read() {
        EntityManager em=entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List banks=em.createQuery("select b from Bank b").getResultList();
        em.getTransaction().commit();
        em.close();
        logger.info("All banks are selected from the DB");
        banks.forEach(System.out::println);
        return banks;
    }

    @Override
    public Bank create(Bank bank) {
        EntityManager em=entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(bank);
        logger.info("Created bank:" + bank);
        em.getTransaction().commit();
        em.close();
        return bank;
    }

    @Override
    public Bank findById(int id) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Bank bank = em.find(Bank.class, id);
        logger.info("Found bank:" + bank);
        em.getTransaction().commit();
        em.close();
        return bank;
    }

    @Override
    public void delete(Bank bank) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Bank bankDB = em.find(Bank.class, bank.getId());
        em.remove(bankDB);
        logger.info("Deleted bank:" + bankDB);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update(Bank bank) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Bank bankDB = em.find(Bank.class, bank.getId());
        bankDB=bank;
        logger.info("Updated bank:" + bank);
        em.getTransaction().commit();
        em.close();
    }
}
*/