package be.kdg.java2.banksystemwebapp.services;

import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.repository.json.JSONSaver;
import be.kdg.java2.banksystemwebapp.repository.springdatajpa.ClientRepository;
import be.kdg.java2.banksystemwebapp.utils.exceptions.JsonFileClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {
    private ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Client findById(Integer clientId) {
        return clientRepository.findById(clientId).orElseThrow();
    }

    @Override
    public Object findAll() {
        return clientRepository.findAll();
    }

    @Override
    public void addClient(Client client) {
        clientRepository.save(client);
    }


    @Override
    public List<Client> findByCreditScoreGreaterThanEqual(int score) {
        return clientRepository.findByCreditScoreGreaterThanEqual(score);
    }

    @Override
    public List<Client> findByCreditScoreAndName(String name, int score) {
        return clientRepository.findByCreditScoreAndName(name,score);
    }
/*
    private EntityRepository<Client> clientRepository;
    private JSONSaver saver;
    private Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    public ClientServiceImpl(EntityRepository<Client> clientRepository, JSONSaver saver) {
        this.clientRepository = clientRepository;
        this.saver = saver;
    }

    @Override
    public Client addClient(Client client) {
        logger.debug("Client is added successful");
        //Client client = new Client(name, creditScore,address);
        return clientRepository.create(client);
    }

    @Override
    public List<Client> getAllClients() {
        logger.debug("All clients are returned");
        try {
            saver.saveClients(clientRepository.read());
        } catch (JsonFileClientException e) {
            e.printStackTrace();
        }
        return clientRepository.read();
    }

    public Client getClientById(int id){
        return clientRepository.findById(id);
    }

    public List<Client> getClientsByNameAndCredit(String name, int creditscore) {
        logger.debug("Clients are filtered");
        List<Client> filtered = clientRepository.read().stream()
                .filter(client -> client.getName().contains(name) && client.getCreditScore() > creditscore)
                .toList();
        try {
            saver.saveClients(filtered);
        } catch (JsonFileClientException e) {
            e.printStackTrace();
        }
        return filtered;
    }

    @Override
    public void saveClientsInJson(List<Client> clients) throws JsonFileClientException {
        saver.saveClients(clients);
    }

 */
}
