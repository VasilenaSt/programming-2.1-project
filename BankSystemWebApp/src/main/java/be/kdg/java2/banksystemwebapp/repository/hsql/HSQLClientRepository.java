package be.kdg.java2.banksystemwebapp.repository.hsql;

import be.kdg.java2.banksystemwebapp.domain.Bank;
import be.kdg.java2.banksystemwebapp.domain.Client;
import be.kdg.java2.banksystemwebapp.domain.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
@Repository
@Profile("hsql")
public class HSQLClientRepository implements EntityRepository<Client> {
    private Logger logger = LoggerFactory.getLogger(HSQLBankRepository.class);

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert clientInserter;

    public HSQLClientRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.clientInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("CLIENTS").usingGeneratedKeyColumns("id");
        logger.debug("Creating Client repository with HSQL...");
    }

    @Override
    public List<Client> read() {
        logger.debug("read clients with HSQL...");
        return jdbcTemplate.query("SELECT * FROM CLIENTS", this::mapRow);
    }

    @Override
    public Client create(Client client) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", client.getName());
        parameters.put("CREDITSCORE", client.getCreditScore());
        parameters.put("ADDRESS", client.getAddress());
        client.setId(clientInserter.executeAndReturnKey(parameters).intValue());
        logger.info("Client with name: " + client.getName() +
                " is saved with id: " + client.getId());
        return client;

    }

    @Override
    public Client findById(int id) {
        logger.debug("find client by id with HSQL...");
        return jdbcTemplate.queryForObject("SELECT * FROM PERSONS WHERE id = ?",
                this::mapRow, id);
    }

    private Client mapRow(ResultSet rs, int i) throws SQLException {
        Client client = new Client(rs.getString("name"),
                rs.getInt("creditscore"),
                rs.getString("address"));
        return client;
    }

    @Override
    public void delete(Client entity) {

    }

    @Override
    public void update(Client entity) {

    }
}
*/