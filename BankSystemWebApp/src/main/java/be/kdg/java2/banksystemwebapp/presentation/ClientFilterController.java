package be.kdg.java2.banksystemwebapp.presentation;

import be.kdg.java2.banksystemwebapp.presentation.dto.FilterClientDTO;
import be.kdg.java2.banksystemwebapp.services.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/filterclients")
public class ClientFilterController {
    private Logger log = LoggerFactory.getLogger(ClientFilterController.class);
    private ClientService clientService;

    public ClientFilterController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    public String showFilteredClients(Model model){
        model.addAttribute("filter", new FilterClientDTO());
        return "filterclients";
    }

    @PostMapping
    public String handleFilteredClients(@Valid @ModelAttribute("filter") FilterClientDTO filter, Model model){

        if(filter.getName().isBlank()){
            model.addAttribute("clients",clientService.findByCreditScoreGreaterThanEqual(filter.getScore()));
        }
        else {
            model.addAttribute("clients",clientService.findByCreditScoreAndName(filter.getName(),filter.getScore()));
            clientService.findByCreditScoreAndName(filter.getName(),filter.getScore()).forEach(System.out::println);
        }

        return "filterclients";
    }
}
