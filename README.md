# Programming 2.1
### Project Name: Bank System Web Application
Author: Vasilena Stoyanova  
JDK version: 16.0.1  
Date: 24 December 2021   


## Domain
#### Bank
Bank entity has a _name_, _bank type_ which is enum (_more information below_), 
_headquarters_ which is the city where the bank's main office is located, 
_CEO_ and a _list of clients_.

#### Client
Client entity has a _name_, _credit score_ which is a number between 300 and0
850, an _address_ and a _list of banks_. One client can be registered in multiple 
banks and a bank has many clients.

#### CEO
Each bank has a CEO entity which represents the chief executive officer of the 
given financial institution and therefore, each officer has a bank that he/she 
manages. A CEO has a _name_, _salary_ and the date when he/she took up office 
(_inOffice_).

#### BankType
BankType is enum, used in Bank class.

##### 1. Central Banks:
Central banks manage the money supply in a single country or a series of 
nations. They supervise commercial banks, set interest rates and control 
the flow of currency.

##### 2. Retail Banks:
Retail banks offer members of the public financial products and
services such as bank accounts, loans, credit cards and insurance.
In some cases, they can set up checking accounts and make loans for
small-scale businesses as well.

##### 3. Commercial Banks:
Commercial banks tend to concentrate on supporting businesses.
Both large corporations and small businesses can turn to commercial
banks if they need to open a checking or savings account, borrow money,
get access to credit or transfer funds to companies in foreign markets.

##### 4. Investment Banks:
Investment Banks manage the trading of stocks, bonds and other securities
between companies and investors. On the other hand, they might focus their
energy on advising individuals and corporations who need financial guidance,
reorganizing companies through mergers and acquisitions, managing investment
portfolios or raising money for certain businesses and the federal government.

##### 5. Cooperative Banks:
Cooperatives can be either retail banks or commercial banks. They’re typically
local or community-based associations whose members help determine how the
business is operated. They’re run democratically, and they offer loans and
banks accounts, among other things.