package be.kdg.java2.banksystemwebapp.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/","","/index"})
public class IndexController {

    @GetMapping
    public String showIndexPage(){
        return "index";
    }

    @GetMapping("/add")
    public String showAddPage(){return "add";}

    @GetMapping("/filter")
    public String showFilterPage(){return "filter";}

}
